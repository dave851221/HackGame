# -*- coding: utf-8 -*-
############
############
### Author : 張代杰
### Date : 2018/05/17
### Usage : Connect two Wii Remotes , one controls the direction and the other controls motor's power.
###         Also connect IR LED , to activate the Wii Remotes to control the car.
############
############
import RPi.GPIO as GPIO
import threading
import cwiid
from time import sleep

def rumble(Player,t):       #令Player震動t次
    for i in range(2*t):
        Player.rumble = 1-i%2
        sleep(.2)

def connect_to_mul_remote():
    global P1
    global P2
    i = 2
    print '搜尋裝置1...'
    while not P1:
        try:
            P1=cwiid.Wiimote()
        except RuntimeError:
            if(i>20):
                quit()
                break
            print 'Error:RuntimeError'
            print 'attempt ' + str(i)
            i+=1
    print '成功連接手把1\n'
    P1.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC      #開啟按鈕模式及傾斜偵測
    P1.led = 1      #Player 1 : 手把第一顆燈亮
    rumble(P1,1)
    
    i = 2
    print '搜尋裝置2...'
    while not P2:
        try:
            P2=cwiid.Wiimote()
        except RuntimeError:
            if(i>10):
                quit()
                break
            print 'Error:RuntimeError'
            print 'attempt ' + str(i)
            i+=1
    print '成功連接手把2\n'
    P2.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC      #開啟按鈕模式及傾斜偵測
    P2.led = 2      #Player 2 : 手把第二顆燈亮
    rumble(P2,1)

def IR_control():
    global Wii_Enable , IR_process , irp
    while(True):
        try:
            if(GPIO.input(IRLED) and Wii_Enable):
                print '關閉Wii控制權限'
                Wii_Enable = False
                P1.led = 0
                P2.led = 0
                threading.Thread(target=rumble , args=(P1,1)).start()
                threading.Thread(target=rumble , args=(P2,1)).start()
            elif(not GPIO.input(IRLED) and not Wii_Enable):
                print '開啟Wii控制權限'
                Wii_Enable = True
                P1.led = 1 if(Direction_Mode == 0) else 2
                P2.led = 2
                threading.Thread(target=rumble , args=(P1,1)).start()
                threading.Thread(target=rumble , args=(P2,1)).start()
            if(irp):
                break
            sleep(0.1)
        except:
            print 'IR_control end'
            break
            
def Set_Mode(Player):
    global Direction_Mode
    if(Player.state['buttons'] - cwiid.BTN_PLUS - cwiid.BTN_MINUS == 0):    #同時按下+和-
        Direction_Mode = 0 if Direction_Mode==1 else 1
        if(Direction_Mode == 0):
            Player.led = 1
        else:
            Player.led = 2
        print '方向模式為:' + {0:'橫向操控',1:'直向操控'}[Direction_Mode]
        rumble(Player,1)
        while(Player.state['buttons'] - cwiid.BTN_PLUS - cwiid.BTN_MINUS == 0):()

def show_acc_value(Player): #不斷顯示各軸調整後之數值(for debug)
    while(True):
        try:
            x = Player.state['acc'][0]  #x軸
            y = Player.state['acc'][1]  #y軸
            z = Player.state['acc'][2]  #z軸
            print 'x:'+str(acc_adjust(x)) + ' y:'+str(acc_adjust(y)) + ' z:'+str(acc_adjust(z))
        except KeyboardInterrupt:
            print '\nfinish!'
            break

def acc_adjust(n):  #output範圍為0~50，限制住手把傾斜角度的範圍
    if n>145:
        return 145-95
    elif n<95:
        return 95-95
    return n-95

def Get_Direction(Player):
#return dist() , keys -> 'direction':0(straight) or 1(left) or 2(right) , 'turn':0~100
    global Direction_Mode
    Set_Mode(Player)
    x = Player.state['acc'][0]      #x軸，可判定一般持把之轉向
    y = Player.state['acc'][1]      #y軸，可判定橫向持把之轉向
    z = Player.state['acc'][2]      #z軸，可判定幅度
    
    if(Direction_Mode==0):
    ### Mode 0 : 橫向持把，需修正判定軸為y軸，並修正幅度軸z軸之靈敏度(提高靈敏度)
        direction = acc_adjust(y)   #橫向持把，需注意判定軸(y軸)的正反向相反(相對於x軸)
        turn = 50-acc_adjust(z)
        turn += 5
        turn = turn*3 if(turn<34) else 100
        if(direction>30):   #左轉
            direction = 1
        elif(direction<20): #右轉
            direction = 2
        else:               #直走
            direction = 0
            turn = 0    #將幅度設為0
            
        #msg = {0:'直走',1:'左轉',2:'右轉'}[direction] + ' 幅度:' + str(turn) + '%'
        #print msg
    
    elif(Direction_Mode==1):
    ### Mode 1 : 直向持把，利用x軸與z軸判斷轉向及幅度
        direction = acc_adjust(x)   #direction(轉向):接收判斷轉向之軸的數值，結果輸出0,1,2分別代表直走,左轉,右轉。
        turn = (50-acc_adjust(z))*2 #turn(幅度):將原先的範圍50~0反轉並擴增成0~100%，代表轉彎的幅度。
        
        if(direction<15):   #左轉
            direction = 1
        elif(direction>35): #右轉
            direction = 2
        else:               #直走
            direction = 0
            turn = 0    #將幅度設為0
        
        #msg = {0:'直走',1:'左轉',2:'右轉'}[direction] + ' 幅度:' + str(turn) + '%'
        #print msg
    else:
        return {'direction':0,'turn':0} #處理例外Error，因為規定mode的值應為0或1
    return {'direction':direction,'turn':turn}

def Get_Power(Player):
    if(not(Player.state['buttons'] & cwiid.BTN_A)):   #按鈕A沒有被按下
        return {'power':0,'forward':True}
    y = Player.state['acc'][1]      #y軸，可判定強度
    z = Player.state['acc'][2]      #z軸，可判定向前或向後
    
    power = acc_adjust(y)       # power = 0~50
    #print "Get_Power: acc_adjust(y)=" + str(power)
    
    ####透過電池、馬達的不同，來調整power的數值
    ###註:利用智慧魔法車，馬達電源4.5V，實測出power<15都不會轉動
    
    # 後退 #
    forward = acc_adjust(z)         # forward(方向):True為向前，False為後退
    if(forward<25): # 全力後退 #
        return {'power':60,'forward':False}
    elif(forward>=25 and power>=0 and power<=13):
        ## 清文:後退的握把手勢應該更靈敏 ##控制後退的power
        power = 3*power + 20 # power : 20~59
        return {'power':power,'forward':False}
        
    # 前進 #
    # power>13 and power<=50
    power = power * 4 - 32  # power>20 and power<=168
    power = 100 if power>100 else power
    return {'power':power,'forward':True}
    
def motor(m,d):
    if(m=='A'):     # motor1:左輪
        if(d=='f'):
            GPIO.output(DIR1, True)
        elif(d=='b'):
            GPIO.output(DIR1, False)
    elif(m=='B'):   # motor2:右輪
        if(d=='f'):
            GPIO.output(DIR2, True)
        elif(d=='b'):
            GPIO.output(DIR2, False)
            
def StartControl():
    global irp
    while(True):
        try:
            Direction = Get_Direction(P1)       # Direction = { 'direction':0~2 , 'turn':0~100 }
            Power = Get_Power(P2)               # Power = { 'power':integer , 'forward':True or False}
            if(Power['power']!=0):
                msg = {True:'向前',False:'向後'}[Power['forward']] + {0:'直走',1:'左轉',2:'右轉'}[Direction['direction']]
                msg += ' 強度:' + str(Power['power']) + '% 幅度:' + str(Direction['turn']) + '%'
                print msg
            if(Power['forward']):
                motor('A','f')
                motor('B','f')
            else:
                motor('A','b')
                motor('B','b')
                
            if (Wii_Enable):
                if(Direction['direction']==0):  # 直走
                    ENA.start(Power['power'])
                    ENB.start(Power['power'])
                elif(Direction['direction']==1):# 左轉
                    ENA.start(Power['power'])
                    ENB.start(Power['power']*(100-Direction['turn'])/100)
                elif(Direction['direction']==2):# 右轉
                    ENB.start(Power['power'])
                    ENA.start(Power['power']*(100-Direction['turn'])/100)
            else:
                ENA.start(0)
                ENB.start(0)
            sleep(0.05)
        except:
            print '【Finish】KeyboardInterrupt'
            irp = True
            while(IR_process.is_alive()):
                pass
            print 'IR_process killed'
            break

##############
###
###  main
###
##############
# ENA:右輪
# ENB:左輪
Direction_Mode = 0  # 方向控制模式，0為橫向握把，1為直向握把
Wii_Enable = True   # 利用紅外線LED去控制wii手把的控制權
P1 = None   # Player1 控制方向
P2 = None   # Player2 控制強度

#GPIO setup
GPIO.setmode(GPIO.BOARD)    # GPIO.BOARD(電路板上接腳號碼) or GPIO.BCM(GPIO後面的號碼)
GPIO.setwarnings(False)

# Pin腳位接口
DIR1 = 31   # 控制Motor1的正轉與反轉(右輪)
DIR2 = 33   # 控制Motor2的正轉與反轉(左輪)
PWM1 = 35   # 控制Motor1的轉速(右輪)
PWM2 = 37   # 控制Motor2的轉速(左輪)
IRLED = 29  # 紅外線接收器的訊號

# Motor1
GPIO.setup(PWM1,GPIO.OUT)   # PWM1
GPIO.setup(DIR1,GPIO.OUT)   # DIR1
# Motor2
GPIO.setup(PWM2,GPIO.OUT)   # PWM2
GPIO.setup(DIR2,GPIO.OUT)   # DIR2
# IR LED
GPIO.setup(IRLED,GPIO.IN)   # IRLED

ENA = GPIO.PWM(PWM1,500)    # Motor1的強度，PWM(pin,frequency)
ENB = GPIO.PWM(PWM2,500)    # Motor2的強度
for i in [DIR1,DIR2]:
    GPIO.output(i,True)

# 等待連接(兩隻手把)，無限迴圈直到都連接上
connect_to_mul_remote()

#show_acc_value(P1) #無限迴圈 顯示各軸調整後之數據

# 開始紅外線偵測
irp = False
IR_process = threading.Thread(target=IR_control , args=())
IR_process.start()

# 主程式，利用兩手把訊息來控制車子的兩個馬達。
StartControl()

GPIO.cleanup()              # 程序結束執行前必須要進行清理
