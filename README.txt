【wii_car_ir.py】

### INSTALLATION: ###
"sudo apt-get install python-cwiid"


###### WIRING: ######
註:樹莓派腳位(Pin)一律以Board表示

<馬達驅動版>
Pin	Motor
4	4V5
6	GND
31	DIR1
33	DIR2
35	PWM1
37	PWM2

電池	Motor
4V5	3V
GND	GND

<紅外線LED>
發射:
長腳---150Ω(棕綠棕)---PIN.4(5V)
短腳---PIN.6(GND)

接收:
長腳---6.2MΩ(藍紅綠)---PIN.4(5V) and PIN.29(GPIO)
短腳---PIN.6(GND)