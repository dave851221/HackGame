**主辦單位：NTUECS**

**活動名稱：程式大賽-HackGame**

**活動日期：2018/05/30**

**活動介紹：程式大賽中的一個環節。雙人利用wii手把控制車體，使其上方的RFID卡片接近RC522讀卡晶片，讀取資料後將於網頁上顯示密碼。**


# 硬體需求
* RPi x2
* Wii Remote x2
* RC522
* 馬達驅動版
* 車殼
* 馬達 x2
* 紅外線LED(發射與接收)
* RFID卡片
* 線材

# 前置作業
## 第一台RPi
**註 : 作為WebServer及CardReader**

### 安裝函式庫：

    sudo pip install eventlet
    sudo pip install flask-socketio

### 接線：

#### RC522
|PIN(Board)|RC522|
| :---: | :---: |
| 17 | VCC |
| 22 | RST |
| 25 | GND |
| 21 | MISO |
| 19 | MOSI |
| 23 | SCK |
| 24 | NSS |
| None | IRQ |

### 執行：

    sudo python webserver.py
    sudo python cardreader.py
    
    
## 第二台RPi
**註 : 連接wii手把並控制車子**

### 安裝函式庫：

    sudo apt-get install python-cwiid

### 接線：

#### 馬達驅動版
|PIN(Board)|馬達驅動版|
| :---: | :---: |
| 4 | 4v5 |
| 6 | GND |
| 31 | DIR1 |
| 33 | DIR2 |
| 35 | PWM1 |
| 37 | PWM2 |

|電池|馬達驅動版|
| :---: | :---: |
| 4v5 | 3V |
| GND | GND |

#### 紅外線LED
##### 發射端：

長腳 --- 150Ω(棕綠棕) --- PIN#4(5V)

短腳 --- PIN#6(GND)


##### 接收端：

長腳 --- 6.2MΩ(藍紅綠) --- PIN#4(5V) and PIN#29(GPIO)

短腳 --- PIN#6(GND)


### 執行：

    sudo python wii_car_ir.py