# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import SimpleMFRC522

class RC522:
    READER = None
    last_id = None
    
    
    def __init__(self):
        self.READER = SimpleMFRC522.SimpleMFRC522()
    
    def Clear_last(self):       # 清空最後一名玩家id的紀錄
        self.last_id = None
    
    def Wait_for_one(self):     # block!! 等待一名玩家感應卡片，並回傳id
        id = self.READER.read_id()
        self.last_id = id
        return str(id)
        
    def Wait_for_other(self):   # block!! 等待一名其他玩家感應卡片，並回傳id
        id = self.READER.read_id()
        while(self.last_id == id):  #模擬do-while
            id = self.READER.read_id()
        self.last_id = id
        return str(id)