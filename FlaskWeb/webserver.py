# -*- coding: utf-8 -*-
import os
import json
from flask import *
from flask_socketio import SocketIO, emit
from multihtml import html
os.chdir('/home/pi/Desktop/HackGame/FlaskWeb')

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True
app.register_blueprint(html)

socketio = SocketIO(app)


@app.route('/card_detected/<id>')
def card_detected(id):
    print '\n【讀卡】- card_detected function -> id : ' + str(id)
    auth_ids = json.load(open('authentication.json'))
    if(auth_ids['ids'].count(id)!=0):
        print '【讀卡】- ID認證成功\n'
        emit('CardDetected' , {'data': 'detected' , 'id':id} ,namespace='/', broadcast=True)
    else:
        print '【讀卡】- 偵測到未註冊的卡片!\n'
    return ''
    
@app.route('/img/<path:filename>')
def image(filename):
    return send_from_directory('./static/img', filename)
@app.route('/gtts/<path:filename>')
def sound_gtts(filename):
    return send_from_directory('./static/sound/tts', filename)
@app.route('/javascript/<path:filename>')
def js(filename):
    return send_from_directory('./templates', filename)
#這邊的函式內容，需要增加至偵測到讀卡時的中斷。
#請勿刪除，要留給card_detection_simulation.html
@socketio.on('card_simulation')
def broadcast_message(message):
    emit('CardDetected' , {'data': message['data']} , broadcast=True) #原封不動傳遞訊息回index.html，使之轉址
    print 'Server Broadcast data:' + str(message['data'])

if __name__ == "__main__":
    socketio.run(app, port=80, host='0.0.0.0')
