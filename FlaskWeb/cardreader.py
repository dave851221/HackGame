# -*- coding: utf-8 -*-
############
############
### Author : DJoke
### Date : 2018/05/15
############
############
import RPi.GPIO as GPIO
from DJoke_RC522 import RC522
import urllib2
from time import sleep

GPIO.setwarnings(False)

reader = RC522()
#Server = "http://djoke.ddns.net"
#Server = "http://127.0.0.1"
Server = ["http://127.0.0.1","http://djoke.ddns.net"]
server = 0

while(True):
    try:
        print "\n【讀卡】- 已開啟讀卡機，請感應卡片:"
        print "\n【Server】"+Server[server]+"\n"
        while(True):
            id = reader.Wait_for_one()
            print 'id:',id
            urllib2.urlopen(Server[server]+"/card_detected/"+id).read()
            sleep(3)
    except KeyboardInterrupt:
        print "\n\nClosing CardReader.py ......"
        break
    except:
        server = 1 if server==0 else 0
        print 'Exception!!\n\n'
        print '\n【Server】-更改Server網址!'

GPIO.cleanup()