//======================================================================
//語音函式
var soundEmbed = null;
//======================================================================
function SoundPlay(filename){
    if (!soundEmbed){
        soundEmbed = document.createElement("embed");
        soundEmbed.setAttribute("src", "/gtts/"+filename+".mp3");
        soundEmbed.setAttribute("hidden", true);
        soundEmbed.setAttribute("autostart", true);
    }
    else{
        document.body.removeChild(soundEmbed);
        soundEmbed.removed = true;
        soundEmbed = null;
        soundEmbed = document.createElement("embed");
        soundEmbed.setAttribute("src", "/gtts/"+filename+".mp3");
        soundEmbed.setAttribute("hidden", true);
        soundEmbed.setAttribute("autostart", true);
    }
    soundEmbed.removed = false;
    document.body.appendChild(soundEmbed);
}