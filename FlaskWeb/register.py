# -*- coding: utf-8 -*-
import json
import RPi.GPIO as GPIO
from DJoke_RC522 import RC522
import urllib2
import os
os.chdir('/home/pi/Desktop/HackGame/FlaskWeb')

GPIO.setwarnings(False)
reader = RC522()
data = json.load(open('authentication.json'))
print 'data:',data

while(True):
    try:
        print "\n【讀卡】- 已開啟讀卡機登錄器，請感應卡片:"
        while(True):
            id = reader.Wait_for_one()
            print 'id:',id
            if(data['ids'].count(id)==0):
                data['ids'].append(id)
                print '\n【新增】新增成功!!'
                print 'id:',id,'\n'
            
    except KeyboardInterrupt:
        print "\n\nSaving ......"
        break

with open('authentication.json', 'w') as outfile:
    json.dump(data, outfile)

GPIO.cleanup()
