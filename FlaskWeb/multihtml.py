# -*- coding: utf-8 -*-
from flask import Blueprint, render_template

html = Blueprint('html', __name__)

#show html : main.html
@html.route('/')
def index():
    return render_template('main.html')
    
#show html : card_detection_simulation.html
@html.route("/sim")
def simulation():
    return render_template('card_detection_simulation.html')