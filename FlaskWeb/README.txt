
### INSTALLATION: ###
"sudo pip install eventlet"
"sudo pip install flask-socketio"

###### WIRING: ######
註:樹莓派腳位(Pin)一律以Board表示

<RC522>
Pin	RC522
17	VCC
22	RST
25	GND
21	MISO
19	MOSI
23	SCK
24	NSS
None	IRQ

####### LOGIC: #######
【webserver.py】

default port:80 (使用putty開啟時需sudo)

接收html request包含的卡片id，並於"authentication.json"內確認是否為註冊過的卡片，是則透過websocket廣播訊息(顯示Hacking讀條及密碼並撥放gtts語音)。


【register.py】

接好RC522並且執行此檔案，可將卡片id註冊至authentication.json，即可利用此卡片執行駭入(webserver.py內會進行判斷，而非cardreader.py)。


【cardreader.py】

default server : 127.0.0.1
secondary server : djoke.ddns.net

接好RC522並且執行此檔後，可將讀卡訊息傳遞至server端的webserver.py，若伺服器無回應則自動切換伺服器。每一次讀到卡都使用sleep(3)來做延遲。
